<?php

namespace StoreBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('active', 'checkbox', ['required' => false])
            ->add('name', 'text')
            ->add('category')
            ->add('manufacturer')
            ->add('model')
            ->add('sku')
            ->add('price')
            ->add('description', 'textarea');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('active')
            ->add('name')
            ->add('category')
            ->add('manufacturer')
            ->add('model')
            ->add('sku')
            ->add('description');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('active')
            ->add('images', 'array', ['template' => 'admin/list/image.html.twig'])
            ->addIdentifier('name')
            ->add('category')
            ->add('manufacturer')
            ->add('model')
            ->add('sku')
            ->add('price')
            ->add('description')
            ->add('_action', null, ['actions' => ['edit' => [], 'delete' => []]]);
    }
}
