<?php

namespace StoreBundle\DataFixtures\ORM;

use StoreBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Создаёт пользователей в БД (в данной случае, одного - админа)
 */
class UserFixtures extends AbstractFixture implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');

        $user = new User();
        $user->setName('Admin')
             ->setUsername('Admin')
             ->setMiddlename('A')
             ->setLastname('Family')
             ->setPassword(
                 $passwordEncoder->encodePassword($user, 'True:Adm1n')
             )
             ->setEmail('example@example.com')
             ->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);
        $this->addReference('Admin', $user);
        $manager->flush();
    }
}
