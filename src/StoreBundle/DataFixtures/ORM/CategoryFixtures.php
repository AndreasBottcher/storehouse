<?php

/**
 *
 */

namespace StoreBundle\DataFixtures\ORM;

use StoreBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Примеры категорий
 */
class CategoryFixtures extends AbstractFixture
{
    public static $names = [
        'Шапки',
        'Пальто',
        'Категория 1',
        'Категория 2',
        'Категория 3',
        'Ноутбуки',
        'Десктопы',
        'ПО',
    ];

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::$names as $index => $name) {
            $category = new Category();
            $category->setName($name)->setDescription($name.': описание категории.');

            $manager->persist($category);
            $this->addReference('category-'.$index, $category);
        }

        $manager->flush();
    }
}
