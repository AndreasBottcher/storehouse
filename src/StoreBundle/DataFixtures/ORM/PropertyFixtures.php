<?php

/*
 * Andrew B.
 */

namespace StoreBundle\DataFixtures\ORM;

use StoreBundle\Entity\Property;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

class PropertyFixtures extends AbstractFixture
{
    public static $propertiesData = [
        ['name' => 'Цвет', 'desc' => 'Цвет продукта'],
        ['name' => 'Вес', 'desc' => 'Вес продукта'],
        ['name' => 'Ширина', 'desc' => 'Ширина продукта'],
        ['name' => 'Глубина', 'desc' => 'Глубина продукта'],
        ['name' => 'Высота', 'desc' => 'Высота продукта'],
        ['name' => 'Угол обзора', 'desc' => 'Угол обзора (для ЖК)'],
        ['name' => 'Цветопередача', 'desc' => 'Глубина цветопередачи'],
        ['name' => 'Загрузка', 'desc' => 'Загрузка СМ (в кг)'],
        ['name' => 'Тип экрана', 'desc' => 'Тип экрана (для ЖК)'],
    ];

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $om)
    {
        foreach (self::$propertiesData as $pKey => $propertyData) {
            $property = new Property();

            $property->setName($propertyData['name'])->setDescription($propertyData['desc']);

            $om->persist($property);
            $this->addReference('property-'.$pKey, $property);
        }

        $om->flush();
    }
}
