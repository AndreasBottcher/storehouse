<?php

namespace StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StoreBundle\Entity\User;

class HomeController extends Controller
{
    /**
     * @Route("/login/", name="login")
     */
    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');

        return $this->render('home/login.html.twig', [
            // last username entered by the user (if any)
            'last_username' => $helper->getLastUsername(),
            // last authentication error (if any)
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in app/config/security.yml
     *
     * @Route("/logout/", name="logout")
     */
    public function logoutAction()
    {
        throw new \Exception('This should never be reached!');
    }

    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $productRepository = $this->getDoctrine()->getRepository('StoreBundle:Product');

        $products = $productRepository->queryLatest()->setMaxResults(12)->getResult();

        return $this->render('home/home.html.twig', [
            'latest_products' => $products,
        ]);
    }
}
