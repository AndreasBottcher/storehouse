<?php

namespace StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use StoreBundle\Entity\Category;
use StoreBundle\Entity\Product;
//use StoreBundle\Entity\Property;

class CatalogController extends Controller
{
    private function getCategories()
    {
        $categoryRepository = $this->getDoctrine()->getRepository('StoreBundle:Category');

        return $categoryRepository->findAll();
    }

    /**
     * @Route("/catalog/", name="catalog")
     */
    public function indexAction()
    {

        return $this->render('catalog/catalog.html.twig', [
            'categories' => $this->getCategories(),
        ]);
    }

    /**
     * @Route("/catalog/{category}/", name="category")
     */
    public function categoryAction(Category $category)
    {
        return $this->render('catalog/category.html.twig', [
            'category' => $category,
            'categories' => $this->getCategories(),
            'products' => $category->getProducts(),
        ]);
    }

    /**
     * @Route("/catalog/{category}/{product}/", name="product")
     */
    public function productAction($category, Product $product)
    {
        $propertyRepository = $this->getDoctrine()->getRepository('StoreBundle:Property');
        $productRepository = $this->getDoctrine()->getRepository('StoreBundle:Product');

        return $this->render('catalog/product.html.twig', [
            'product' => $product,
            'categories' => $this->getCategories(),
            'properties' => $propertyRepository->findAll(),
            'properties_values' => $product->getProperties(),
            'prev' => $productRepository->queryPrevious($product)->getOneOrNullResult(),
            'next' => $productRepository->queryNext($product)->getOneOrNullResult(),
        ]);
    }

}
